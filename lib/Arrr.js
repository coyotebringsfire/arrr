var tpb = require('thepiratebay');

const DEFAULT_CATEGORY = '200';
const DEFAULT_SORT = "SEEDS_DESC";
const DEFAULT_PAGE = 0;
const DEFAULT_LIMIT = 5;
const DEFAULT_TITLE = "";

module.exports = class {
  constructor() {}
  search(title=DEFAULT_TITLE, category=DEFAULT_CATEGORY, sortBy=DEFAULT_SORT, page=DEFAULT_PAGE, limit=DEFAULT_LIMIT) {
    return tpb.search(title, {
      category: category,
      page: 0,
      orderBy: sortBy
    }).then( function(results) {
      return results.slice(page*limit, page*limit+limit)
    });
  }
}
