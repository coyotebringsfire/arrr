# arrr
search the pirate bay from the command line

## Installation
```
$ npm install -g arrr-cli
```

## Usage
```
Usage: /Users/aumkara/.node/bin/arrr [options] <title>

Options:
  -c, --category  the category to search                        [default: "200"]
  -s, --sort-by   how to sort results                    [default: "SEEDS_DESC"]
  -p, --page      which page of results to show                     [default: 0]
  -l, --limit     the number of results to return                   [default: 5]
  -h, --help      Show help                                            [boolean]
```

```
$ arrr star wars
Star.Wars.The.Force.Awakens.2015.UNMARKED.TS.XviD-VAiN           1.29 GiB 2253 01-28 05:58 magnet:?xt=urn:btih:0cd44b51116d374739cc678125e10ade95e9a0e2&dn=Star.Wars.The.Force.Awakens.2015.UNMARKED.TS.XviD-VAiN&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969
Star.Wars.Episode.VII..2015.720p.BRRip.x264.AAC-ETRG             1.01 GiB 625  03-22 22:34 magnet:?xt=urn:btih:127ba7b1d987b042e1a12d97633161ec59a93888&dn=Star.Wars.Episode.VII..2015.720p.BRRip.x264.AAC-ETRG&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969
Star.Wars.Episode.VII.The.Force.Awakens.2015.BRRip XViD ETRG     1.43 GiB 579  03-22 22:09 magnet:?xt=urn:btih:1b55b9f6a602a38e227d22e2bf36513182eb011c&dn=Star.Wars.Episode.VII.The.Force.Awakens.2015.BRRip+XViD+ETRG&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969
Star.Wars.Episode.VII.The.Force.Awakens.2015.1080p.BluRay-JYK    3.48 GiB 555  03-23 05:37 magnet:?xt=urn:btih:361c3b67f562f2e7f4d10ea22022d26beda88286&dn=Star.Wars.Episode.VII.The.Force.Awakens.2015.1080p.BluRay-JYK&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969
Star Wars Episode I, II, III, IV, V, VI - Complete Saga George L 6.33 GiB 501  09-30 2014  magnet:?xt=urn:btih:8479a6d8bd55904907ef3c69833be0981323f505&dn=Star+Wars+Episode+I%2C+II%2C+III%2C+IV%2C+V%2C+VI+-+Complete+Saga+George+L&tr=udp%3A%2F%2Ftracker.openbittorrent.com%3A80&tr=udp%3A%2F%2Fopen.demonii.com%3A1337&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Fexodus.desync.com%3A6969
```

## Output
The results show the file name, size, number of seeders, upload date, and magnet url

## Sort Options
* NAME_DESC - by name descending
* NAME_ASC - by name ascending
* DATE_DESC - by date descending
* DATE_ASC - by date ascending
* SIZE_DESC - by size descending
* SIZE_ASC - by size ascending
* SEEDS_DESC - by seeders descending [default]
* SEEDS_ASC - by seeders ascending
* LEECHES_DESC - by leeches descending
* LEECHES_ACS - by leeches ascending

## Category Options
* 0 - All
* 100 - Audio
* 200 - Video
* 300 - Applications
* 400 - Games
* 500 - Porn
* 600 - Other
