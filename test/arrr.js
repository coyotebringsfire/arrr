var exec = require('child_process').exec,
    should = require('should');

const TEST_MOVIE = "star wars";

describe("arrr", function() {
  this.timeout(10000);
  it("should search the default category, limit, and page by title", function(done) {
    exec(`./arrr.js ${TEST_MOVIE}`, function(err, stdout, stderr) {
      if(err) {
        done(err);
      } else {
        stdout = stdout.split('\n');
        stdout.length.should.equal(6);
        done();
      }
    });
  });
  describe("category", function() {
    const TEST_CATEGORY = 700;
    for( let c of ["c", "-category"] ) {
      it(`-${c} should return an error with an invalid category`, function(done) {
        exec(`./arrr.js -${c} INVALID_CATEGORY ${TEST_MOVIE}`, function(err, stdout, stderr) {
          should(err).be.ok;
          should(stderr).be.ok;
          stderr.should.match(/invalid category/i);
          done();
        });
      });
      it(`-${c} should search the specified category, default limit, and default page by title`, function(done) {
        exec(`./arrr.js -${c} ${TEST_CATEGORY} ${TEST_MOVIE}`, function(err, stdout,stderr) {
          stdout.split('\n').length.should.equal(6);
          done(err);
        });
      });
    }
  });
  describe("limit", function() {
    const TEST_LIMIT = 7;
    for( let l of ["l", "-limit"] ) {
      it(`-${l} should return an error with an invalid limit`, function(done) {
        exec(`./arrr.js -${l} INVALID_LIMIT ${TEST_MOVIE}`, function(err, stdout, stderr) {
          should(err).be.ok;
          should(stderr).be.ok;
          stderr.should.match(/invalid limit/i);
          done();
        });
      });
      it(`-${l} should search the default category, specified limit, and default page by title`, function(done) {
        exec(`./arrr.js -${l} ${TEST_LIMIT} ${TEST_MOVIE}`, function(err, stdout,stderr) {
          stdout.split('\n').length.should.equal(TEST_LIMIT+1);
          done(err);
        });
      });
    }
  });
  describe("page", function() {
    const TEST_PAGE = 2;
    for( let p of ["p", "-page"] ) {
      it(`-${p} should return an error with an invalid page`, function(done) {
        exec(`./arrr.js -${p} INVALID_PAGE ${TEST_MOVIE}`, function(err, stdout, stderr) {
          should(err).be.ok;
          should(stderr).be.ok;
          stderr.should.match(/invalid page/i);
          done();
        });
      });
      it(`-${p} should search the default category, default limit, and specified page by title`, function(done) {
        exec(`./arrr.js -${p} ${TEST_PAGE} ${TEST_MOVIE}`, function(err, stdout,stderr) {
          stdout.split('\n').length.should.equal(5+1);
          done(err);
        });
      });
    }
  });
  describe("sort", function() {
    const TEST_SORT = "NAME_DESC";
    for( let s of ["s", "-sort-by"] ) {
      it(`-${s} should return an error with an invalid sort`, function(done) {
        exec(`./arrr.js -${s} INVALID_SORT ${TEST_MOVIE}`, function(err, stdout, stderr) {
          should(err).be.ok;
          should(stderr).be.ok;
          stderr.should.match(/invalid sort/i);
          done();
        });
      });
      it(`-${s} should search the default category, default limit, and default page by title sorted according to option`, function(done) {
        exec(`./arrr.js -${s} ${TEST_SORT} ${TEST_MOVIE}`, function(err, stdout,stderr) {
          stdout.split('\n').length.should.equal(5+1);
          done(err);
        });
      });
    }

  });
  describe("-h or --help", function() {
    for( let h of ["h", "-help"] ) {
      it(`-${h} should print usage`, function(done) {
        exec(`./arrr.js -${h}`, function(err, stdout, stderr) {
          done(err);
        });
      });
    }
  });

});

describe("Arrr", function() {
  it("should expose a class constructor with a search method", function() {
    var Arrr = require('../lib/Arrr');
    Arrr.should.be.type("function");
    var arrr = new Arrr();
    arrr.search.should.be.type("function");
  });
});
