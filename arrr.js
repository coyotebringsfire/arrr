#!/usr/bin/env node

var columnify = require('columnify'),
    chalk     = require('chalk'),
    pkg       = require('./package.json');

var Arrr = require('./lib/Arrr');

const SORTBY = {
  "NAME_DESC": 1,
  "NAME_ASC": 2,
  "DATE_DESC": 3,
  "DATE_ASC": 4,
  "SIZE_DESC": 5,
  "SIZE_ASC": 6,
  "SEEDS_DESC": 7,
  "SEEDS_ASC": 8,
  "LEECHES_DESC": 9,
  "LEECHES_ACS": 10
};

const DEFAULT_CATEGORY = '200';
const DEFAULT_SORT = "SEEDS_DESC";
const DEFAULT_PAGE = 0;
const DEFAULT_LIMIT = 5;

var program = require('yargs')
    .usage('Usage: $0 [options] <title>')
    .alias("c", "category")
    .nargs('c', 1)
    .describe("c", "the category to search")
    .default("c", DEFAULT_CATEGORY)
    .alias("s", "sort-by")
    .nargs('s', 1)
    .describe("s", "how to sort results")
    .default("s", DEFAULT_SORT)
    .alias("p", "page")
    .nargs('p', 1)
    .describe("p", "which page of results to show")
    .default("p", DEFAULT_PAGE)
    .alias("l", "limit")
    .nargs('l', 1)
    .describe("l", "the number of results to return")
    .default("l", DEFAULT_LIMIT)
    .help('h')
    .alias('h', 'help')
    .epilog(`Version ${pkg.version}\nLicensed under ${pkg.license}`)
    .argv;

if( isNaN(parseInt(program.c)) ) {
  console.error(`invalid category ${program.c}`);
  process.exit(1);
}

if( isNaN(parseInt(program.l)) ) {
  console.error(`invalid limit ${program.l}`);
  process.exit(1);
}

if( isNaN(parseInt(program.p) ) ) {
  console.error(`invalid page ${program.p}`);
  process.exit(1);
}

if( SORTBY[program.s] === undefined ) {
  console.error(`invalid sort ${program.s}`);
  process.exit(1);
}

program.title = program._.join(' ');
program.sortBy = SORTBY[program.sortBy];

var arrr = new Arrr();
arrr.search(program.title, program.category, program.sortBy, program.page, program.limit)
  .then( function(results) {
    for( let result of results ) {
      result.name = chalk.green(result.name);
      result.size = chalk.black(result.size);
      result.seeders = chalk.cyan(result.seeders);
      result.uploadDate = chalk.blue(result.uploadDate);
      result.magnetLink = chalk.magenta(result.magnetLink);
    }
    if( results.length === 0 ) {
      console.log("No Results");
    } else {
      console.log( columnify(results, { columns: ["name", "size", "seeders", "uploadDate", "magnetLink"], showHeaders: false }) );
    }
  }).catch(function(err){
    console.log(err);
  });
